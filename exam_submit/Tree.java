/**********************************************************
*Exam Intro to Java
*file: Tree.java
*
*compilation: javac Tree.java
*
*Author: Aleksandr Ten
************************************************************/



public class Tree {
  String name = "none";
  int count = 0;
  boolean infected = true;
  
  public Tree (String newName, int newCount, boolean newBool) {
    name = newName;
    count = newCount;
    infected = newBool;
  }//END Tree constructor
  
}//END class Tree
