/**********************************************************
*Exam Intro to Java
*file: TreeFactory.java
*
*compilation: javac TreeFactory.java
*
*Author: Aleksandr Ten
************************************************************/




import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;


public class TreeFactory {
  //declare array list
  public static ArrayList<Tree> treeList = new ArrayList<Tree>();
  
  
  public void startTrees() {
    catchAndCreateTrees();
    displayData();
  }//END startTrees()
  
  
  
  public void catchAndCreateTrees() {
	//read from trees.txt and create instance depending on tree name and if "infected"
    String fileName = "trees.txt";
    File theFile = new File (fileName);
    
    
    try {
      Tree currentTree;
      Scanner scanner = new Scanner(theFile);
      
      //declare variables to display later
      int numOfConifer = 0;
      int numOfDeciduous = 0;
      int numOfUnknown = 0;
      
      //loop through the scanned data
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String treeString = line;
        String delims = "[|]";
        String[] treeParts = treeString.split(delims);
        
        //if tree name is fir or scotchpine or cedar or whitefir, create Conifer object depending on "infected"
        if ( (treeParts[0].equals("Fir")) || (treeParts[0].equals("ScotchPine")) || (treeParts[0].equals("Cedar")) || (treeParts[0].equals("WhiteFir")) ) {
          //if tree is marked as healthy
          if (treeParts[2].equals("Healthy")) {
            currentTree = new Conifer (treeParts[0], Integer.parseInt(treeParts[1]), false);
            numOfConifer = numOfConifer + Integer.parseInt(treeParts[1]);
          }//END if
          // if tree is infected
          else {
            currentTree = new Conifer (treeParts[0], Integer.parseInt(treeParts[1]), true);
            numOfConifer = numOfConifer + Integer.parseInt(treeParts[1]);
          }//END else if
        }//END if
        
      //if tree name is unknown, create Unknown object depending on "infected"
        else if (treeParts[0].equals("Unknown")) {
          if (treeParts[2].equals("Healthy")) {
            currentTree = new Unknown (treeParts[0], Integer.parseInt(treeParts[1]), false);
            numOfUnknown = numOfUnknown + Integer.parseInt(treeParts[1]);
          }//END if
          else{
            currentTree = new Unknown (treeParts[0], Integer.parseInt(treeParts[1]), true);
            numOfUnknown = numOfUnknown + Integer.parseInt(treeParts[1]);
          }//END else if
        }//END else if
        
      //in all other cases, create Deciduous object depending on "infected"
        else {
          if (treeParts[2].equals("Healthy")) {
            currentTree = new Deciduous (treeParts[0], Integer.parseInt(treeParts[1]), false);
            numOfDeciduous = numOfDeciduous + Integer.parseInt(treeParts[1]);
          }//END if
          else{
            currentTree = new Deciduous (treeParts[0], Integer.parseInt(treeParts[1]), true);
            numOfDeciduous = numOfDeciduous + Integer.parseInt(treeParts[1]);
          }//END else
        }//END else
        
        //add to array list
        treeList.add(currentTree);
        //System.out.println(treeList); //for checking if .add() works
      }//END while
      
      scanner.close();
      System.out.println("Number of conifer trees: " + numOfConifer);
      System.out.println("Number of deciduous trees: " + numOfDeciduous);
      System.out.println("Number of unknown trees: " + numOfUnknown);
    }//END try
      
	catch (FileNotFoundException e) {
		System.out.println("error. file is not found");
	}//END catch

  }//END catchAndCreateTrees()
  
  
  
  public void displayData() {
	Iterator<Tree> treeIterator = treeList.iterator();
	
	//declare variables
	int numInfected = 0;
	int numHealthy = 0;
    
	//loop through data
    while (treeIterator.hasNext()) {
      Tree tree = treeIterator.next();
      
      //if boolean infected is true
      if (tree.infected == true ) {
        numInfected = numInfected + tree.count;
        destroyTrees();
      }//END if
      
      if (tree.infected == false ) {
        numHealthy = numHealthy + tree.count;
        harvestTrees();
      }//END if
      
    }//END while
    
    System.out.println("Total number of infected trees: " + numInfected);
    System.out.println("Total number of healthy trees: " + numHealthy);

  }//END displayData()
  
  
  public void destroyTrees() {
	//System.out.println("destroyed");
  }//END
  
  public void harvestTrees() {
	//System.out.println("harvested");
  }//END
  
  
}//END class TreeFactory




























