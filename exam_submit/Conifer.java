/**********************************************************
*Exam Intro to Java
*file: Conifer.java
*
*compilation: javac Conifer.java
*
*Author: Aleksandr Ten
************************************************************/


public class Conifer extends Tree {
  public Conifer (String newName, int newCount, boolean newBool) {
    super (newName, newCount, newBool);
  }//END Conifer()
  
}//END class Conifer
