/**********************************************************
*Exam Intro to Java
*file: Deciduous.java
*
*compilation: javac Deciduous.java
*
*Author: Aleksandr Ten
************************************************************/



public class Deciduous extends Tree {
  public Deciduous (String newName, int newCount, boolean newBool) {
    super (newName, newCount, newBool);
  }//END Deciduous()
	  
}//END class Deciduous
