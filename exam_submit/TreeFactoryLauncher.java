/**********************************************************
*Exam Intro to Java
*file: TreeFactoryLauncher.java
*
*compilation: javac TreeFactoryLauncher.java
*execution: java -cp . TreeFactoryLauncher
*
*Author: Aleksandr Ten
************************************************************/


public class TreeFactoryLauncher {
  public static void main(String args[]) {
    TreeFactory tree_factory1 = new TreeFactory();
    tree_factory1.startTrees();
  }//END main()
}//END class SoftwareLauncher