/**********************************************************
*Assignment 2 Intro to Java
*file: EmployeeLauncher.java
*
*compilation: javac EmployeeLauncher.java
*execution: java -cp . EmployeeLauncher
*
*Author: Aleksandr Ten
************************************************************/


public class EmployeeLauncher {
  public static void main(String args[]) {
    EmployeeCompany empl_company1 = new EmployeeCompany();
    empl_company1.startEmployees();
  }//END main()
}//END class EmployeeLauncher
