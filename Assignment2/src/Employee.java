/**********************************************************
*Assignment 2 Intro to Java
*file: Employee.java
*Author: Aleksandr Ten
************************************************************/


public class Employee {
  //declare variables to which we assign data when create a new object
  int id = -1;
  String last_name = "none";
  String first_name = "none";
  int salary = -1;
  String employee_type = "none";
  
  //new Employee object constructor
  public Employee(int id_num, String l_name, String f_name, int salar, String emp_type) {
    id = id_num;
    last_name = l_name;
    first_name= f_name;
    salary = salar;
    employee_type = emp_type;
  }//END Employee constructor
  
  //print info about an employee
  public void showData() {
    System.out.println("My name is " + first_name + last_name + ". My id is " + id + ". My salary is " + salary + ", and I am a " + employee_type);
  }//END showData()
  
}//END class Employee
