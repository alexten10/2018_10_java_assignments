/**********************************************************
*Assignment 2 Intro to Java
*file: EmployeeCompany.java
*Author: Aleksandr Ten
************************************************************/


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;
import java.text.DecimalFormat;



public class EmployeeCompany {
  //create an array list
  public static ArrayList<Employee> employeeList = new ArrayList<Employee>();
  
  
  
  public void startEmployees() {
    //System.out.println ("Creating employees");
    readTxtFileAndCreateEmployees();
    showSalaries();
  }//END startEmployees()
	
  
  //method for reading data from a txt file and add that data into an array
  //NOTE that txt file must have no empty lines in it, otherwise an error
  protected void readTxtFileAndCreateEmployees() {
    //scan txt file and create employees based on info in the txt file
    String filename = "Employees.txt";
    File file = new File(filename);
    
    
    // try block contains set of statements where an exception can occur
    /*If an exception occurs in try block then the control of 
     *execution is passed to the corresponding catch block.
     */
    try {
      Scanner scanner = new Scanner(file);
      Employee employee;
      
      while (scanner.hasNextLine()) {
    	String line = scanner.nextLine();
    	String employeeString = line;
    	String delims = "[,]";
    	String[] employeeDataParts = employeeString.split(delims);
    	
    	if (employeeDataParts[4].equals("Clerk")) {
    	  employee = new Clerk (Integer.parseInt(employeeDataParts[0]),employeeDataParts[1],employeeDataParts[2],Integer.parseInt(employeeDataParts[3]),employeeDataParts[4]);
    	}//END if
    	
    	else if (employeeDataParts[4].equals("Manager")) {
      	  employee = new Manager (Integer.parseInt(employeeDataParts[0]), employeeDataParts[1], employeeDataParts[2], Integer.parseInt(employeeDataParts[3]), employeeDataParts[4]);
    	}//END else if
    	
    	else {
          employee = new Other (Integer.parseInt(employeeDataParts[0]), employeeDataParts[1], employeeDataParts[2], Integer.parseInt(employeeDataParts[3]), employeeDataParts[4]);
      	}//END else if
    	
    	employeeList.add(employee);
    	//System.out.println("I added " + line); //to check if method works
        //System.out.println(employee);
    	//employee.showData();
    	
      }//END while
      
      scanner.close();
      
    }//END try
    
    // catch block is where we handle the exceptions from try block
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }//END catch
    
  }//END readTxtFileAndCreateEmployees()
	
	
  //method to get info about salaries from array list and display it
  protected void showSalaries() {
	//loop through array list
    Iterator<Employee> employeeIterator = employeeList.iterator();
    
    int i = 1;
    int totalSalary = 0;
    int totalClerkSalary = 0;
    int totalManagerSalary = 0;
    
    while (employeeIterator.hasNext()) {
      Employee employee = employeeIterator.next();
      totalSalary = totalSalary + employee.salary;
      
      if(employee.employee_type.equals("Clerk")) {
        totalClerkSalary = totalClerkSalary + employee.salary;
      }
      
      if(employee.employee_type.equals("Manager")) {
        totalManagerSalary = totalManagerSalary + employee.salary;
      }
      
      i = i + 1;
    }//END while
    //show total salary amount
    System.out.println("Total salary: " + totalSalary + " .");
    
    //show federal tax on total salary amount, formated into 2 digits after . point ( ex. 2344.22, NOT 2344.2264673)
    double totalFederalTax = totalSalary * 0.45;
    String pattern = "######.##";
    DecimalFormat decimalFormat = new DecimalFormat(pattern);
    String formattedTotalFederalTax = decimalFormat.format(totalFederalTax);
    System.out.println("Total Federal Tax: " + formattedTotalFederalTax + " .");
    
    //print salary of all clerks
    System.out.println("Total Clerks' Salary: " + totalClerkSalary + " .");
    
    //print salary of all managers
    System.out.println("Total Managers' Salary: " + totalManagerSalary + " .");
	
	  
	  
  }//END showSalaries()
	
  
  
  
}//END class EmployeeCompany


























