/**********************************************************
*Assignment 2 Intro to Java
*file: Manager.java
*Author: Aleksandr Ten
************************************************************/


public class Manager extends Employee {
  public Manager (int id_num, String l_name, String f_name, int salar, String emp_type) {
    super(id_num, l_name, f_name, salar, emp_type);
  }// END Manager constructor
}//END class Manager