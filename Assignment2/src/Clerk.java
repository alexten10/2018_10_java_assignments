/**********************************************************
*Assignment 2 Intro to Java
*file: Clerk.java
*Author: Aleksandr Ten
************************************************************/


public class Clerk extends Employee {
  public Clerk (int id_num, String l_name, String f_name, int salar, String emp_type) {
    super(id_num, l_name, f_name, salar, emp_type);
  }// END Clerk constructor
}//END class Clerk
