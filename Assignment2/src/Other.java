/**********************************************************
*Assignment 2 Intro to Java
*file: Other.java
*Author: Aleksandr Ten
************************************************************/


public class Other extends Employee {
  public Other (int id_num, String l_name, String f_name, int salar, String emp_type) {
    super(id_num, l_name, f_name, salar, emp_type);
  }// END Other constructor
}//END class Other