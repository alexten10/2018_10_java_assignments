/**********************************************************
*Assignment 1 Intro to Java
* 
*compilation: javac Player.java
*
*players set two random numbers here
*
**Author: Aleksandr Ten
************************************************************/



package assignment1;




public class Player {
	  //declare variables that will hold random number values
	  int number1 = 0;
	  int number2 = 0;
	  
	  public void guess() {
		//get random numbers from 1 to 69
	    number1 = (int) ((Math.random() * 69) + 1);
	    number2 = (int) (Math.random() * 69) + 1;
	    
	    //print selected by random numbers
	    System.out.println ("My numbers are " + number1 + " and " + number2);
	    
	    
	  }//END guess()
	  
	}//END class Player