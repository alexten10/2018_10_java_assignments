/**********************************************************
*Assignment 1 Intro to Java
* 
*compilation: javac GameStarter.java
*execution: java GameStarter
*
*holds main() method that starts game
*
**Author: Aleksandr Ten
************************************************************/




package assignment1;




public class GameStarter {
	  public static void main (String[] args) {
	    GuessGame game = new GuessGame();
	    game.startGame();
	    
	  }//END main()
	}//END class GameStarter
