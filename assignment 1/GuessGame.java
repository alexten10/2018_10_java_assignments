
/**********************************************************
*Assignment 1 Intro to Java
* 
*compilation: javac GuessGame.java
*
*game procedures are here
*
**Author: Aleksandr Ten
************************************************************/





package assignment1;



public class GuessGame {
	  //3 variables for 3 game players
	  Player player1;
	  Player player2;
	  Player player3;
	  
	  
	  //create 3 players as objects
	  public void startGame() { //begin game
	    player1 = new Player();
	    player2 = new Player();
	    player3 = new Player();
	    
	    
	    //variables to hold players' guess numbers
	    int guessp1_1 = 0;
	    int guessp1_2 = 0;
	    
	    int guessp2_1 = 0;
	    int guessp2_2 = 0; 
	    
	    int guessp3_1 = 0;
	    int guessp3_2 = 0;
	    
	    
	    //variables to hold if any player is a winner(flag)
	    boolean p1isRight = false;
	    boolean p2isRight = false;
	    boolean p3isRight = false;
	    
	    //PC chooses numbers that players should guess
	    int targetNumber1 = (int) (Math.random() * 69 + 1);
	    int targetNumber2 = (int) (Math.random() * 69 + 1);
	    System.out.println("I am taking 2 random numbers from 1 to 69");
	    
	    
	    
	    while(true) {
	    	//print the numbers that PC has chosen
	    	System.out.println("Numbers to guess are " + targetNumber1 + " and " + targetNumber2);
	    	
	    	//call method guess() for each player (they guess random numbers)
	    	player1.guess();
	    	player2.guess();
	    	player3.guess();
	    	
	    	
	    	//assign each player's guessed numbers to variables
	    	guessp1_1 = player1.number1;
	    	guessp1_2 = player1.number2;
	    	System.out.println("Player1 guessed " + guessp1_1 + " and " + guessp1_2);
	    	
	    	guessp2_1 = player2.number1;
	    	guessp2_2 = player2.number2;
	    	System.out.println("Player2 guessed " + guessp2_1 + " and " + guessp2_2);
	    	
	    	guessp3_1 = player3.number1;
	    	guessp3_2 = player3.number2;
	    	System.out.println("Player3 guessed " + guessp3_1 + " and " + guessp3_2);
	    	
	    	
	    	//if guessed by PC numbers match with player's, select flag to true
	    	//matching both combinations of possible matches (ex. 3 - 45 is the same as 45 - 3)
	    	if ((guessp1_1 == targetNumber1 && guessp1_2 == targetNumber2) || (guessp1_1 == targetNumber2 && guessp1_2 == targetNumber1)) {
	    		p1isRight = true;
	    	}//END if
	    	
	    	if ((guessp2_1 == targetNumber1 && guessp2_2 == targetNumber2) || (guessp2_1 == targetNumber2 && guessp2_2 == targetNumber1)) {
	    		p2isRight = true;
	    	}//END if
	    	
	    	if ((guessp3_1 == targetNumber1 && guessp3_2 == targetNumber2) || (guessp3_1 == targetNumber2 && guessp3_2 == targetNumber1)) {
	    		p3isRight = true;
	    	}//END if
	    	
	    	
	    	//variant 1
	    	/***************************************
	    	//if any flag is true for any of the players, print message and stop loop
	    	if (p1isRight || p2isRight || p3isRight) {
	    		System.out.println("There is a winner!!!");
	    		System.out.println("Player1 is a winner? " + p1isRight);
	    		System.out.println("Player2 is a winner? " + p2isRight);
	    		System.out.println("Player3 is a winner? " + p3isRight);
	    		System.out.println("GAME OVER. Goodbye!");
	    		break;//stop loop
	    	} else {
	    		System.out.print("No winner. Next trial.");
	    	}//END if/else
	    	*******************************************/
	    	
	    	
	    	//variant 2
	    	//check if any player has flag true - give message and break
	    	//if no true flag found - give message and loop again
	    	if (p1isRight) {//if player1's flag is true
	    		System.out.println("Player1 is a winner!!!");
	    		System.out.println("GAME OVER. Goodbye!");
	    		break;
	    	} else if (p2isRight) {
	    		System.out.println("Player2 is a winner!!!");
	    		System.out.println("GAME OVER. Goodbye!");
	    		break;
	    	} else if (p3isRight) {
	    		System.out.println("Player3 is a winner!!!");
	    		System.out.println("GAME OVER. Goodbye!");
	    		break;
	    	} else {//if no true flag is found
	    		System.out.println("No winner. Next trial.");
	    		System.out.println("");//for better readability of the results
	    	}//END if/else if / else
	    	
	    }//END while loop
	    
	  }//END startGame()
	  
	}//END class GuessGame